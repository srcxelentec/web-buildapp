/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },
  'GET /partial/:name': 'UsersController.getPartial',
  'GET /api/all_users': 'UsersController.getAllUsers',
  'GET /api/all_orders': 'UsersController.getAllOrders',
  'POST /api/remove_order': 'UsersController.deleteOrder',
  'POST /api/prove_order': 'UsersController.proveOrder',
  'POST /api/return_order': 'UsersController.returnOrder',




  'POST /api/ban_user': 'UsersController.banUser',

  'POST /api/user/singup': 'UsersController.reg',
  'POST /api/user/login': 'UsersController.login',
  'POST /api/user/logout': 'UsersController.logout',
  'PUT /api/user/edit': 'UsersController.edit',
   'PUT /api/user/get_user': 'UsersController.showUser',
    'PUT /api/user/my_profile': 'UsersController.showMe',
     'POST /api/user/add_skill': 'UsersController.addSkill',
         'POST /api/user/remove_skill': 'UsersController.removeSkill',
	    'GET /api/user/skills': 'UsersController.getAllSkills',
	      'POST /api/user/upload': 'UsersController.uploadFiles',
	          'POST /api/user/push_me': 'UsersController.pushMe',

	        'POST /api/user/verify_payment': 'SkillsController.verifyPayment',
	      'GET /api/user/verify_payment': 'SkillsController.checkDates',

	       'POST /api/order/new': 'OrdersController.add',
	       'POST /api/order/get_all': 'OrdersController.getAll',
	       'POST /api/order/get_one': 'OrdersController.getOne',
	       'POST /api/order/follow': 'OrdersController.follow',
	       'POST /api/order/unfollow': 'OrdersController.unfollow',
	       'POST /api/order/mark': 'OrdersController.mark',
	        'POST /api/order/close': 'OrdersController.close',
		 'POST /api/order/get_mine': 'OrdersController.getMine',
		  'POST /api/order/get_followed': 'OrdersController.getFollowed',


		   'POST /api/admin/get_all_users': 'OrdersController.getAllUsers',
		      'POST /api/admin/remove_order': 'OrdersController.remove',
		         'POST /api/admin/remove_user': 'OrdersController.removeUser'
  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
