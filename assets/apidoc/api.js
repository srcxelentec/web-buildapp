/**
 * @api {post} /api/user/singup Регистрация/Отправка СМС
 * @apiName Registration
 * @apiGroup User
 *
 * @apiParam {String} phone Телефон
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
*{
 * "data": "message sent"
*}
 @apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/singup */


/**
 * @api {post} /api/user/login Вход/подтверждение телефона ( по умолчанию код 0000)
 * @apiName Login
 * @apiGroup User
 *
 * @apiParam {String} phone Телефон
 *  @apiParam {String} code Код подтверждения
 *  @apiParam {String} os Опирационная система ("android" или "ios") 
 *  @apiParam {String} push_token Push токен устройства
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
*{
*  "id": 3,
 * "phone": "1",
 * "code": "0000",
*  "token": "VBDV5OUYD77QVPLITA2WNNWTKTKNT6CXSVXT7EWPBJKANBVZWQWRON3VQBKQXG5XGPYNUHVLG426XNIDL4722M5MWTNG74F227DGSHSFY",
*  "userskills": []
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/login */

/**
 * @api {post} /api/user/logout Выход/Logout
 * @apiName Logout
 * @apiGroup User
 *
 *  @apiParam {String} token  токен пользователя
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
*{
*{
*  "status": "ok"
*}
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/logout */



/**
 * @api {put} /api/user/edit Редактирование профиля
 * @apiName Edit profile
 * @apiGroup User
 *
 * @apiParam {String} token токен пользователя
 *  @apiParam {String} name Имя пользователя
 *  @apiParam {Number} city id города
 *  @apiParam {String} photo URL фотографии пользователя
 * @apiParamExample {json} Request-Example:
*{
 *"token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
 *"name":"myName",
* "city":12,
* "photo":"http.....jpg"
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/edit */

/**
 * @api {post} /api/user/add_skill Добавить навык
 * @apiName Add skill
 * @apiGroup User
 *
 * @apiParam {String} token токен пользователя
 *  @apiParam {Number} skill id навыка
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
*"skill":2
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/add_skill */

/**
 * @api {post} /api/user/remove_skill Удалить навык
 * @apiName Remove skill
 * @apiGroup User
 *
 * @apiParam {String} token токен пользователя
 *  @apiParam {Number} skill id навыка
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
*"skill":2
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/remove_skill */

/**
 * @api {post} /api/user/upload Загрузить фото
 * @apiName Upload file
 * @apiGroup User
 *
 *  @apiParam {File} images изображение ( возможна отправка массивом)
 * @apiSuccessExample Success-Response:
*{
 * "data": [
 *   "uploads/images/64b13ae4-cecf-4e39-a9a3-054e569d229f.js",
 *   "uploads/images/637c94e4-76f1-472c-b68b-4ae49bdb0e0b.sql"
  *]
*}
 */



/**
 * @api {put} /api/user/get_user Профиль пользователя
 * @apiName Get user profile
 * @apiGroup User
 *
 * @apiParam {String} token токен пользователя
 *  @apiParam {Number} id id пользователя
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
*"id":2
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/get_user */





/**
 * @api {put} /api/user/my_profile Мой профиль
 * @apiName My profile
 * @apiGroup User
 *
 * @apiParam {String} token токен пользователя
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
*}
* @apiSuccessExample Success-Response:
*{
 * "id": 2,
*  "phone": "123",
*  "code": "0000",
 * "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
 * "name": "myName",
 * "city": 12,
*  "photo": "132",
 * "userskills": [
*    {
*      "skill": 3
 *   },
 *  {
 *     "skill": 2
 *   },
 *   {
 *     "skill": 1
 *   },
 *   {
 *     "skill": 4
  *  }
*  ]
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/my_profile */



/**
 * @api {get} /api/user/skills Перечень навыков
 * @apiName Skills list
 * @apiGroup User
 *
 * @apiParam {String} token токен пользователя

* @apiSuccessExample Success-Response:
*[
*  {
*    "id": 2,
*    "name": "skill 1"
*  },
*  {
*    "id": 3,
*    "name": "skill 2"
*  },
*  {
*    "id": 4,
*    "name": "skill 3"
*  }
*]
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/user/skills */


/**
 * @api {post} /api/order/new Новая заявка
 * @apiName Add order
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 *  @apiParam {String} name Название обяьвления 
 * @apiParam {Number} city id города 
 * @apiParam {Array} skills массив id навыков
 *  @apiParam {String} text Содержание обяьвления 
 *  @apiParam {Array} photos Массив URL-ов фото заявки 
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "name":"order name",
* "city":12,
* "skills":[1,2,3],
* "text":"order text",
* "photos":["123","345","567"]
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/new  */

/**
 * @api {post} /api/order/get_all Получение списка заявок ( с возможностью фильтрации и пагинацией)
 * @apiName Get orders list
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} [city] id города 
 * @apiParam {Number} [skill] id навыка
 * @apiParam {Boolean} [closed] статус заявок
 * @apiParam {Number} page номер страницы (1,2,3 .... )
 * @apiParam {Number} limit количество записей на странице
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "page":1,
* "limit":100,
* "skill":2,
* "city":12
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/get_all  */

/**
 * @api {post} /api/order/get_one Получение заявки по id
 * @apiName Get order
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} id id заявки
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "id":74
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/get_one  */

/**
 * @api {post} /api/order/follow Подписаться на заявку
 * @apiName Follow order
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} id id заявки
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "id":74
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/follow  */

/**
 * @api {post} /api/order/unfollow Отписаться от заявки
 * @apiName Unfollow order
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} id id заявки
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "id":74
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/unfollow  */

/**
 * @api {post} /api/order/mark Оценить исполнителя
 * @apiName Mark user
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} id id заявки
 * @apiParam {Number} user_id id исполнителя
 * @apiParam {Number} mark оценка (0-5)
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/mark  */


/**
 * @api {post} /api/order/close Закрыть заявку/Удалить
 * @apiName Close order
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} id id заявки
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "id":74
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/close  */



/**
 * @api {post} /api/order/get_mine Мои заявки
 * @apiName My orders
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя 
 * @apiParam {Number} page номер страницы 
 * @apiParam {Number} limit количество записей на странице


 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
*   "page":1,
* "limit":100
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/get_mine  */



/**
 * @api {post} /api/order/get_followed Заявки, на которые подписан
 * @apiName Followed orders
 * @apiGroup Order
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} page номер страницы 
 * @apiParam {Number} limit количество записей на странице
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
*   "page":1,
* "limit":100
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/order/get_followed  */




/**
 * @api {post} /api/admin/get_all_users Пользователи
 * @apiName Users
 * @apiGroup Admin
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} page номер страницы 
 * @apiParam {Number} limit количество записей на странице
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
*   "page":1,
* "limit":100
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/admin/get_all_users  */



/**
 * @api {post} /api/admin/remove_order Удалить заявку
 * @apiName Remove order
 * @apiGroup Admin
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} id id заявки
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "id":74
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/admin/remove_order  */



/**
 * @api {post} /api/admin/remove_user Удалить пользователя
 * @apiName Remove user
 * @apiGroup Admin
 *
 * @apiParam {String} token токен пользователя
 * @apiParam {Number} id id пользователя
 * @apiParamExample {json} Request-Example:
*{
* "token": "L34CYRECGQFZMFK7DMLBJCK7BFLFKXV2MOTGOPYGC5OVBEEI677OEO6BZYTWTIG3AZNSYV5OQ7B2TGLSF2XN5566IVOJSK4D3I3DP4EVX",
* "id":74
*    
*}
 *@apiSampleRequest http://buildapp.dev-xelentec.com:1341/api/admin/remove_user  */

