
'use strict';

var myApp = angular.module('myApp', ['ui.router','ui.bootstrap']);


myApp.run([
  '$rootScope','$state',function ($rootScope,$state){
    // console.log('sds');
    //$rootScope.signOut=function()
    //{
    //
    //  $state.go('login')
    //  sessionStorage.setItem('user',null);
    //}
  }]);
myApp.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/home");
  //
  // Now set up the states
  $stateProvider

    .state('login', {
      url: "/login",
      templateUrl: "partial/login",
      controller: 'loginCtrl'
    })
    .state('home', {
      url: '/home',
      abstract:false,
      templateUrl: 'partial/index',
      controller: 'homeCtrl'
    }).state('users', {
      url: '/users',
      abstract:false,
      templateUrl: 'partial/users',
      controller: 'usersCtrl'
    })



});

myApp.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
});
