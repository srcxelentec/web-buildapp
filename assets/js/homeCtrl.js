
'use strict';

var app = angular.module('myApp');
app. controller('homeCtrl', ['$scope','$location','$rootScope','$http',"$state" ,"$uibModal",function($scope,$location,$rootScope,$http,$state,$uibModal) {
  $scope.allItems=[];
  $scope.itemsPerPage=8;
  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };
  $scope.pageChanged = function() {

  };
  $scope.$watch('currentPage',function(newValue,oldValue) {
    $scope.restorePaging();
  });
  $scope.$watch('all',function(newValue,oldValue) {

    if(newValue!=oldValue) {
      if (newValue) {

        $scope.allItems  = $scope.allItems.filter(function (elem) {
          return !elem.moderated;
        });
        $scope.totalItems = $scope.allItems.length;
        $scope.currentPage = 1;
        $scope.restorePaging();
      }
      else {
      $scope.allItems= $scope.cash;
        $scope.totalItems = $scope.allItems.length;
        $scope.currentPage = 1;
        $scope.restorePaging();
      }
    }
  });
  $scope.editItem=function(item)
  {
    $scope.item=item;

  }
  $scope.deleteItem=function(item)
  {
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'myModalContent2.html',
      controller: 'ModalInstanceCtrl2',
      size: 'md',
      resolve: {
        config: function() {
            return {type:'заявку',name:item.name};
          return {};
        }
      }
    });

    modalInstance.result.then(function (action) {

      if(action)
      {


        $http({
          method: 'POST',
          url: '/api/remove_order/',
          data:{id:item.id}
        }).success(function (result) {
          $scope.allItems= $scope.allItems.filter(function(elem){ return elem.id!=item.id});
          $scope.totalItems = $scope.allItems.length;
          $scope.currentPage = 1;
          $scope.restorePaging();

        });

      }

    }, function () {
    });
  }
  $scope.returnItem=function(item)
  {
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'myModalContent3.html',
      controller: 'ModalInstanceCtrl3',
      size: 'md',
      resolve: {
        config: function() {
          return {type:'заявку',name:item.name};
          return {};
        }
      }
    });

    modalInstance.result.then(function (action) {
console.log(action);
      $http({
        method: 'POST',
        url: '/api/return_order/',
        data:{id:item.id,return_text:action.return_text}
      }).success(function (result) {
        console.log(result);
        for (var i=0;i<$scope.allItems.length;++i)
        {
          if($scope.allItems[i].id==result.id)
          {

            $scope.allItems[i]=result;
          }
        }
        for (var i=0;i<$scope.items.length;++i)
        {
          if($scope.items[i].id==result.id)
          {
            console.log('changed');
            $scope.items[i]=result;
          }
        }




      });

    }, function () {
    });
  }
  $scope.populateItems=function() {
    $http({
      method: 'GET',
      url: '/api/all_orders/',
    }).success(function (result) {
      $scope.allItems = result;
      $scope.cash=$scope.allItems;

      $scope.totalItems = $scope.allItems.length;
      $scope.currentPage = 1;
      console.log($scope.totalItems);
      console.log($scope.allItems);
      $scope.restorePaging();


    });


  }

  $scope.proveOrder=function(id) {
    $http({
      method: 'POST',
      url: '/api/prove_order/',
      data:{id:id}
    }).success(function (result) {
      console.log(result);
      for (var i=0;i<$scope.allItems.length;++i)
      {
        if($scope.allItems[i].id==result.id)
        {

          $scope.allItems[i]=result;
        }
      }
      for (var i=0;i<$scope.items.length;++i)
      {
        if($scope.items[i].id==result.id)
        {
          console.log('changed');
          $scope.items[i]=result;
        }
      }




    });



  }
  $scope.populateItems();
  $scope.restorePaging=function()
  {
    if($scope.allItems) {
      $scope.totalItems = $scope.allItems.length;
      $scope.items = $scope.allItems.slice($scope.itemsPerPage * ($scope.currentPage - 1), $scope.itemsPerPage * ($scope.currentPage - 1) + $scope.itemsPerPage);
    }
  }
}]);
app.controller('ModalInstanceCtrl2', function ($scope, $uibModalInstance, config) {
  console.log(config);
  $scope.type=config.type;
  $scope.name=config.name;
  $scope.ok = function () {
    $uibModalInstance.close('delete');
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
app.controller('ModalInstanceCtrl3', function ($scope, $uibModalInstance, config) {
  console.log(config);
  $scope.type=config.type;
  $scope.name=config.name;
  $scope.ok = function () {
    $uibModalInstance.close({return_text:$scope.return_text});
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
