
'use strict';

var app = angular.module('myApp');
app. controller('usersCtrl', ['$scope','$location','$rootScope','$http',"$state" ,function($scope,$location,$rootScope,$http,$state) {
  $scope.allItems=[];
  $scope.itemsPerPage=8;
  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };
  $scope.pageChanged = function() {
    //console.log('Page changed to: ' + $scope.currentPage);
  };
  $scope.$watch('currentPage',function(newValue,oldValue) {

    $scope.restorePaging();
  });

  $scope.populateItems=function() {
    $http({
      method: 'GET',
      url: '/api/all_users/',
    }).success(function (result) {
      $scope.allItems = result;


      $scope.totalItems = $scope.allItems.length;
      $scope.currentPage = 1;
      $scope.restorePaging();


    });


  }

  $scope.banUser=function(id,term) {
    $http({
      method: 'POST',
      url: '/api/ban_user/',
      data:{id:id,term:term}
    }).success(function (result) {
      console.log(result);
      for (var i=0;i<$scope.allItems.length;++i)
      {
        if($scope.allItems[i].id==result.id)
        {

          $scope.allItems[i]=result;
        }
      }
      for (var i=0;i<$scope.items.length;++i)
      {
        if($scope.items[i].id==result.id)
        {
          console.log('changed');
          $scope.items[i]=result;
        }
      }




    });
    console.log(id);
    console.log(term);


  }
  $scope.populateItems();
  $scope.restorePaging=function()
  {

    if($scope.allItems) {
      $scope.totalItems = $scope.allItems.length;
      $scope.items = $scope.allItems.slice($scope.itemsPerPage * ($scope.currentPage - 1), $scope.itemsPerPage * ($scope.currentPage - 1) + $scope.itemsPerPage);
    }
  }

}]);
