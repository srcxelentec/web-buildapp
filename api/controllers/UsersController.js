/**
 * UsersController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
		reg: function(req, res){
var date= new Date();
		var phone=req.param('phone');

				var code = "0000";
if(req.param('phone').substring(1,7)!='7(000)'){code=Math.round(1000 + Math.random() * (9999 - 1000));}
		Users.findOne({phone:phone}).exec(function(err, user){


				if (err){
					return res.badRequest(err);
				}
				if(user){
if(!user.lastSms){
user.lastSms=new Date();
user.lastSms.setSeconds(user.lastSms.getSeconds() - 100000000020);
}
var current_date = new Date();
var countSms=0;
user.lastSms.setSeconds(user.lastSms.getSeconds() + 120);
if((user.countSms==1)&&(user.lastSms>current_date)){
var ms=user.lastSms-current_date;
return res.ok({err_code:"timeout",error: "До следующей попытки осталось", ms:ms},201);
}else {

user.lastSms.setSeconds(user.lastSms.getSeconds() - 120 + 3600);
if((user.countSms==2)&&(user.lastSms>current_date)){
var ms=user.lastSms-current_date;
return res.ok({err_code:"timeout",error: "До следующей попытки осталось", ms:ms},201);
}
}
if(user.countSms==null||user.countSms==2)
countSms=1;
else
countSms=user.countSms+1;



					Users.update({id:user.id},{code: code,lastSms:date, countSms:countSms}).exec(function (err, updated){

					if (err){
						return res.badRequest(err);
					}
					else
					{
	  var http = require('http');
http.post = require('http-post');
http.post('http://smsc.ru/sys/send.php', { login:'Fomarom', psw:'4342004', mes:code, phones:req.param('phone').substring(1), charset:'utf-8' }, function(resp){
});

                                return res.ok({data: "message sent"},201);
					}
				});


				}else{
				Users.create({phone: phone, code: code,lastSms:date}).exec(function (err, created){

					if (err){
						return res.badRequest(err);
					}

					else{
					  	  var http = require('http');
http.post = require('http-post');
http.post('http://smsc.ru/sys/send.php', { login:'Fomarom', psw:'4342004', mes:code, phones:req.param('phone').substring(1), charset:'utf-8' }, function(resp){
});

                                return res.ok({data: "message sent"},201);
					}

				});

				}




		});

	},




	pushMe: function(req, res){

		var id=req.param('id');


		Users.findOne({id:id}).exec(function(err, user){


				if (err){
					return res.badRequest(err);
				}
				if(user){



					    var push=require("./../services/pushCustomer.js");
			 var token = user.push_token;

                                var push_type=user.os;
					 var title="Hello!";

					 push.sendMessage(push_type,token,title,'body', function(err, result) {
                                if (err){
console.log(err);
					return res.badRequest(err);
				}
console.log(result);
				           return res.serialize(result,'login',201);

 });




				}
				else
                               return res.ok({error: "нет такого"},201);





		});

	},


			login: function(req, res){

		var phone=req.param('phone');
                 var code=req.param('code');
		  var os=req.param('os');
		   var push_token=req.param('push_token');
				var randtoken = require('rand-token').generator({chars: 'base32'});
				var token = randtoken.generate(105);

		Users.findOne({phone:phone, code:code}).exec(function(err, user){


				if (err){
					return res.badRequest(err);
				}
				if(user){

					Users.update({id:user.id},{token: token, countSms:0, push_token:push_token, os:os}).exec(function (err, updated){

					if (err){
						return res.badRequest(err);
					}
				Users.findOne({id:updated[0].id}).populate("userskills").exec(function(err, final_user){
				  if(push_token && push_token!=null && os && os!=null)
				  Users.update({id:{ '!': user.id},push_token:push_token, os:os },{ push_token:null, os:null}).exec(function (err, dublicated_updated){
				 	});



                                return res.serialize(final_user,'login',201);
	});
				});
				}
				else
                               return res.ok({error: "Логин и пароль не совпадают"},201);





		});

	},
	logout: function(req, res){




		var token=req.param('token');



		Users.findOne({token:token}).exec(function(err, user){


				if (err){
					return res.badRequest(err);
				}
				if(user){

					Users.update({id:user.id},{push_token:null, os:null}).exec(function (err, updated){

					if (err){
						return res.badRequest(err);
					}


                                return res.ok({"status":"ok"},201);

				});
				}
				else
                               return res.ok({error: "Ошибка авторизации"},201);





		});

	},
		edit: function(req, res){

		var token=req.param('token');



		Users.findOne({token:token}).exec(function(err, user){


				if (err){
					return res.badRequest(err);
				}
				if(user){







					Users.update({id:user.id},req.body).exec(function (err, updated){

					if (err){
						return res.badRequest(err);
					}

					if(req.body.name)
					Mark.update({marked_by:user.id},{user_from_name:req.body.name}).exec(function (err, updated_marks){
					});


	     Users.findOne({id:user.id}).populate('userskills').exec(function (err, updated){

					if (err){
						return res.badRequest(err);
					}


				if(req.body.push_token && req.body.os)
				  Users.update({id:{ '!': user.id},push_token:req.body.push_token, os:req.body.os },{ push_token:null, os:null}).exec(function (err, dublicated_updated){
				 	});


				 return res.serialize(updated,'login',201);


				});

				});
				}
				else
                               return res.ok({error: "Ошибка авторизации"},402);





		});

	},

		showUser: function(req, res){

		var token=req.param('token');



		Users.findOne({token:token}).exec(function(err, user){


				if (err){
					return res.badRequest(err);
				}
				if(user){


	     Users.findOne({id:req.param('id')}).populate('userskills').populate('marks').exec(function (err, updated){

					if (err){
						return res.badRequest(err);
					}
					Mark.find({user:req.param('id')}).sort('createdAt DESC').exec(function (err, marks){
					  updated.marks=marks;
				 return res.serialize(updated,'profile',201);

	});
				});

				}
				else
                               return res.ok({error: "Ошибка авторизации"},402);





		});

	},


		showMe: function(req, res){
		var token=req.param('token');
             	Users.findOne({token:token}).populate('userskills').populate('marks').populate('orders').populate('followed_orders').exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				  Mark.find({user:user.id}).sort('createdAt DESC').exec(function (err, marks){
					  user.marks=marks;
			 return res.serialize(user,'myprofile',201);
			 });
                         	}
				else
                               return res.ok({error: "Ошибка авторизации"},402);
			});

	},
		addSkill: function(req, res){
		var token=req.param('token');
             	Users.findOne({token:token}).populate('userskills').exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				  UserSkill.create({owner:user,skill:req.param('skill')}).exec(function(err, new_skill){
			 return res.ok({"status":"ok"},201);
			 });
                         	}
				else
                               return res.ok({error: "Ошибка авторизации"},402);
			});

	},
		removeSkill: function(req, res){
		var token=req.param('token');
             	Users.findOne({token:token}).populate('userskills').exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){

				  UserSkill.destroy({owner:user.id,skill:req.param('skill') }).exec(function (err) {
                            return res.ok({"status":"ok"},201);
});

                         	}
				else
                               return res.ok({error: "Ошибка авторизации"},402);
			});

	},
		getAllSkills: function(req, res){



				 Skills.find().exec(function (err, skills) {
                       return res.serialize(skills,'skill',201);
});




	},
	getPartial:function(req,res){
    res.view(req.param('name'), { layout: null});
  },
  getAllUsers:function(req,res){
    Users.find().exec(function (err, users) {
        var now=new Date();
      for(var i=0;i<users.length;++i)
      {
        if(users[i].ban_expire)
        {
          var tempDate=new Date(users[i].ban_expire);
        }

        if(users[i].ban_expire && tempDate>now )
        {
          users[i].banned=true;
        }
      }
        return res.json(users);
    });
  },
  banUser:function(req,res){

    Users.findOne({id:req.param('id')}).exec(function(err, user){


      if (err){
        return res.badRequest(err);
      }
      if(user) {
        var banDate = new Date();
        var term = req.param('term');
        console.log(req.param('term'));
        if (!term)
          term = 1;
        if (term == 999)
        {
          banDate=new Date(Date.parse("January 1, 2100"));
          console.log(banDate)
        }else
        banDate.setDate(banDate.getDate() + parseInt(term));
        //banDate=(banDate.getDate()+5);
        Users.update({id:user.id},{ ban_expire:banDate}).exec(function (err,newUser){
          if(newUser)
          newUser[0].banned=true;
          return res.json(newUser[0]);
        });

      }
    });

  },getAllOrders:function(req,res){

    Orders.find().exec(function (err, orders) {
      return res.json(orders);
    });
  },
  deleteOrder:function(req,res){
    Orders.destroy({id:req.body.id}).exec(function (err) {
      res.json('ok');
    });

  },proveOrder:function(req,res){

    Orders.findOne({id:req.param('id')}).exec(function(err, user){


      if (err){
        return res.badRequest(err);
      }
      if(user) {

        Orders.update({id:user.id},{ moderated:1, returned:0,return_text:''}).exec(function (err,newUser){

          return res.json(newUser[0]);
        });

      }
    });

  },returnOrder:function(req,res){

    Orders.findOne({id:req.param('id')}).exec(function(err, user){


      if (err){
        return res.badRequest(err);
      }
      if(user) {

        Orders.update({id:user.id},{ returned:1,return_text:req.body.return_text}).exec(function (err,newUser){
/////////////
//console.log(user);

          Users.findOne({id:user.owner}).exec(function(err, tempUser){

if(tempUser) {
  var push = require("./../services/pushCustomer.js");
  var push_token = tempUser.push_token;
  var push_type = tempUser.os;
  var title = {};

  title.alert = 'Вашу заявку вернули:' + req.body.return_text;
  title.order_id = user.id;
  title.order_title = user.name;
  title.type = "new follower";

  push.sendMessage(push_type, push_token, title, 'body', function (err, result) {
    ;
  });
}
          });
////////////







          return res.json(newUser[0]);
        });

      }
    });

  },
	uploadFiles: function(req, res){

var str_arr=[];


				req.file('images').upload({	dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/images')
					},function (err, uploadedFiles) {
					  if (err) return res.negotiate(err);


					async.each(['0'], function(file, callback){
						var index = 0;

						while (index < uploadedFiles.length){
						  	var abs_path = uploadedFiles[index]['fd'];
						  	var img_name = abs_path.substr(abs_path.lastIndexOf('/')+1);
						  str_arr.push('uploads/images/'+img_name);
						  	index++;
						}
						callback();
					}, function(err){
					    // if any of the file processing produced an error, err would equal that error
					    if( err ) {
					    	return res.badRequest(err);
					    }

				    	return res.ok({data:str_arr}, 200);

					});
				});


	}


};

