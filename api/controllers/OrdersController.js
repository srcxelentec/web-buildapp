/**
 * OrdersController
 *
 * @description :: Server-side logic for managing orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
		add: function(req, res){
		var token=req.param('token');
             	Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				 
				  Orders.create({owner:user,name:req.param('name'),text:req.param('text'),city:req.param('city')}).exec(function(err, created){
				  var images=req.param('photos');
				 
				var skills=req.param('skills');
				
				
				
				var each = require('async-each-series');
each(skills, function(skill, nextSkill) {
OrderSkill.create({skill:skill, order:created}).exec(function(err, new_skill){
						nextSkill();
							 });
}, function (err) {




					async.each(images, function(image, callback){
						

						
						Orderphoto.create({url:image, order:created}).exec(function(err, photo){
						callback();
							 });
						
						
					}, function(err){
					    // if any of the file processing produced an error, err would equal that error
					    if( err ) {
					    	return res.badRequest(err);
					    }
	                  Orders.findOne({id:created.id}).populate('photos').populate('skills').exec(function(err, order){
			      var push=require("./../services/pushCustomer.js");
			      
			      
			    UserSkill.find({skill:skills}).populate("owner").exec(function(err, skilled_users){
			    
			        if( err ) {
					    	return res.badRequest(err);
					    }
			     
			      	for(i=0;i<skilled_users.length;i++){
				  if( skilled_users[i].owner.push_token&&skilled_users[i].owner.os && skilled_users[i].owner.city==order.city && skilled_users[i].owner.id!=user.id){
			      var token = skilled_users[i].owner.push_token;
					var push_type=skilled_users[i].owner.os;
					 var title=new Object();
					// title.alert='В городе появилась новая заявка для ваших навыков "'+ order.name+'"';
					  title.alert= order.name;
					 title.order_id=order.id;
					 title.order_title=order.name;
					 title.type="new order";
					 push.sendMessage(push_type,token,title,'body', function(err, result) {
					 });
					 
				  }	 
				}
			      
				  });
			    
			    
	                  return res.serialize(order,'me',201);
				    	//return res.ok({data:str_arr}, 200);
					   
					});   
					});
				      
					
			 
});
		
					
					
				
		
			 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Ошибка авторизации"},402);
			});
		
	},
	
	getAll: function(req, res){
	
				
				 var criteria=new Object();
				 if(req.param('city'))
				   criteria.city=req.param('city');
				  if(req.param('closed'))
				   criteria.closed=req.param('closed');
				   
				 
			
				  
				  
			var waterfall = require('async-waterfall');
			
			
			waterfall([
  function( callback){
	 if(req.param('skill'))
	   OrderSkill.find({skill:req.param('skill')}).exec(function(err, o_skills){
				   criteria.id=[];
			console.log(o_skills);
				   for(o_num=0;o_num<o_skills.length; o_num++)
				     criteria.id.push(o_skills[o_num].order);
				   
				     callback(null, 'done');
				   	 }); 
				 else
    callback(null, 'done');
  }
], function (err, pseudo_result) {
				  Orders.find(criteria).paginate({page:req.param('page'), limit: req.param('limit')}).populate('skills').populate('owner').populate('photos').populate('followers').sort('createdAt DESC').exec(function(err, orders){
				  if (err){
					return res.badRequest(err);
				}
				Orders.find(criteria).populate('owner').populate('photos').exec(function(err, all_orders){
				    if (err){
					return res.badRequest(err);
				}  
				  var resp=new Object();
				resp.orders=orders;
				for(i=0;i<orders.length;i++)
 resp.orders[i].followers_count= orders[i].followers.length;


 
				 
				 
				 resp.count=all_orders.length;
				
			 return res.serialize(resp,'me',201);
			 });
				 	 }); 
				  
                         });	
		
		
	},
	getOne: function(req, res){
		req.body.name
				
				  Orders.findOne({id:req.param('id')}).populate('owner').populate('skills').populate('photos').exec(function(err, order){
				  if(order)
				Followers.find({order:order.id}).sort('createdAt DESC').populate('user').exec(function(err, followers){
			 return res.serialize({order:order,followers:followers, followers_count:followers.length },'me',201);
			 });
			 });
				  
				  
                         
		
	},
		follow: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				  Orders.findOne({id:req.param('id')}).populate("owner").exec(function(err, order){
				  
				Followers.create({user:user,order:order}).exec(function(err, follower){
				     
				   var push=require("./../services/pushCustomer.js");
				   var push_token = order.owner.push_token;
					var push_type=order.owner.os;
					 var title=new Object();
					// title.alert='На вашу заявку подписались "'+order.name+'"';
					  title.alert='Новый ответ по вашей заявке';
					 title.order_id=order.id;
					 title.order_title=order.name;
					  title.type="new follower";
					 push.sendMessage(push_type,push_token,title,'body', function(err, result) {
					 });
					 
			      return res.ok({status: "Following"},200);
			  });
			 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Ошибка авторизации"},402);
			});
		
	},
		unfollow: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				  Orders.findOne({id:req.param('id')}).exec(function(err, order){
				  
				Followers.destroy({user:user.id,order:order.id}).exec(function(err){
			      return res.ok({status: "Following stopped"},200);
			  });
				
				
			 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Ошибка авторизации"},402);
			});
		
	},
			mark: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				    
				  
				  Orders.findOne({id:req.param('id')}).exec(function(err, order){
				  
				if(  order.marked)  
				       return res.ok({error: "Оценка по текущей заявке ужеeach выставлена"},200);  
				    else
				    Mark.create({user:req.param('user_id'), mark:req.param('mark'), user_from_name:user.name, marked_by:user.id,user_from_photo:user.photo, order_id:order.id, order_skill:order.skill, order_name:order.name}).exec(function(err, marked){  
				      Orders.update({id:order.id},{marked:true}).exec(function (err, updated){
					
					    return res.ok({status: "Marked successful"},200); 
					
				      }); 
				      }); 
				 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Ошибка авторизации"},402);
			});
		
	},
			close: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				    
				  
				  Orders.findOne({id:req.param('id')}).exec(function(err, order){
				  
				if(  order.owner!=user.id)  
				       return res.ok({error: "Доступ запрещен"},200);  
				    else
				        Orders.update({id:order.id},{closed:true}).exec(function (err, updated){
						Followers.destroy({order:order.id}).exec(function(err){
				  OrderSkill.destroy({order:order.id}).exec(function(err){
				    Orders.destroy({id:order.id}).exec(function(err){
					
			    return res.ok({status: "Order closed"},200); 
			   });
					  
					});
			 });
				 
					   
					
				      }); 
				
				 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Ошибка авторизации"},402);
			});
		
	},
	
		getMine: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				 var criteria=new Object();
				criteria.owner=user.id;
				  Orders.find(criteria).paginate({page:req.param('page'), limit: req.param('limit')}).populate('owner').populate('skills').populate('followers').sort('createdAt DESC').exec(function(err, orders){
				    Orders.find(criteria).exec(function(err, all_orders){
					  var resp=new Object();
				 resp.orders=orders;
	for(i=0;i<orders.length;i++)
 resp.orders[i].followers_count= orders[i].followers.length;


				 resp.count=all_orders.length;
				
			 return res.serialize(resp,'me',201);
			 });
				 });  
				  
                         	}
				else 
                               return res.ok({error: "Ошибка авторизации"},402);
			});
		
	},
			getFollowed: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user){
				 var criteria=new Object();
				criteria.user=user.id;
				  Followers.find(criteria).exec(function(err, followed){
				  
				var order_criteria=new Object();
				order_criteria.id=[];
				for( i=0; i<followed.length; i++)
				  order_criteria.id.push(followed[i].order);
				//return res.ok(order_criteria);

				 Orders.find(order_criteria).paginate({page:req.param('page'), limit: req.param('limit')}).populate('owner').populate('skills').populate('followers').sort('createdAt DESC').exec(function(err, orders){
				    Orders.find(order_criteria).exec(function(err, all_orders){
				   	  var resp=new Object();
				 resp.orders=orders;
				 	for(i=0;i<orders.length;i++)
 resp.orders[i].followers_count= orders[i].followers.length;

				 resp.count=all_orders.length;
				
			 return res.serialize(resp,'me',201);
			  });
				   
				});
			 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Ошибка авторизации"},402);
			});
		
	},
		getAllUsers: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user && user.role==1){
			
				 Users.find({role:0}).paginate({page:req.param('page'), limit: req.param('limit')}).exec(function(err, users){
				    Users.find({role:0}).exec(function(err, users){
				   	  var resp=new Object();
				 resp.users=users;
				 resp.count=users.length;
				
				   
			 return res.serialize(resp,'mini_profile',201);
			 	 });
			 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Доступ запрещен"},402);
			});
		
	},
	remove: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user && user.role==1){
	Followers.destroy({order:req.param('id')}).exec(function(err){
	    OrderSkill.destroy({order:req.param('id')}).exec(function(err){
					Orders.destroy({id:req.param('id')}).exec(function(err){
			      return res.ok({status: "Removed"},200);
			  });
					  });
			 });
				  
				  
                         	}
				else 
                               return res.ok({error: "Доступ запрещен"},402);
			});
		
	},
	removeUser: function(req, res){
		var token=req.param('token');
		Users.findOne({token:token}).exec(function(err, user){
			if (err){
					return res.badRequest(err);
				}
				if(user && user.role==1){
				  
				   Users.destroy({id:req.param('id')}).exec(function(err){  
                                });
			   
			      Followers.destroy({user:req.param('id')}).exec(function(err){
				 });
				
				Mark.destroy({user:req.param('id')}).exec(function(err){
				  
				  }); 
				UserSkill.destroy({owner:req.param('id') }).exec(function (err) {
				  });
				  Mark.update({marked_by:req.param('id')},{marked_by:null}).exec(function (err, updated_marks){
					});
					  
					    Orders.find({owner:req.param('id')}).exec(function(err, all_orders){
					   
					   	async.each(all_orders, function(order, callback){
						
OrderSkill.destroy({order:order.id}).exec(function(err){
						 Followers.destroy({order:order.id}).exec(function(err){
						   callback();
						 });
						  });
						
					}, function(err){
					     if( err ) {
					    	return res.badRequest(err);
					    }
					    Orders.destroy({owner:req.param('id')}).exec(function(err){
					  return res.ok({status: "Removed"},200);
			   });    
			
					     
					  });
				 
				
				
			     
					  
					});
			 
				  
				  
                         	}
				else 
                               return res.ok({error: "Доступ запрещен"},402);
			});
		
	}
	
};

