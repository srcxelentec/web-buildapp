/**
* OrderSkill.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var map = {
   me: {
        skill: {}
    },
      myprofile: {
        skill: {}
    }
};
module.exports = {
tableName: 'OrderSkill',
  attributes: { 
    
    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    order: {
	  columnName: 'order_id',
      model: 'orders'
    },
    skill: {type: 'integer'},
    getGrpFields: function(grp) {
        return map[grp];
    }
  }
};

