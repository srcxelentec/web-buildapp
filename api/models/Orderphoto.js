/**
* Orderphoto.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


var map = {
    me: {
        id: {},
        url: {}
    }
};

module.exports = {

  tableName: 'Order_photos',
  attributes: {
  	id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    url: {type: 'string'},
    order: {
	  columnName: 'order',
      model: 'orders'
    },
    getGrpFields: function(grp) {
        return map[grp];
    }
}
};
