/**
* Skills.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


var map = {
    skill: {
        id: {},
       name: {}
    }
};

module.exports = {

  tableName: 'skills',
  attributes: {
  	id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
     name: {type: 'string'},

    getGrpFields: function(grp) {
        return map[grp];
    }
}
};
