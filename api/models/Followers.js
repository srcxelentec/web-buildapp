/**
* Followers.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


var map = {
    me: {
        user: {}
    },
    myprofile: {
        order: {}
    }
};

module.exports = {

  tableName: 'Followers',
  attributes: {
  	id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
 order: {
	  columnName: 'order_id',
      model: 'orders'
    },
    user: {
	  columnName: 'user_id',
      model: 'users'
    },
    getGrpFields: function(grp) {
        return map[grp];
    }
}
};
