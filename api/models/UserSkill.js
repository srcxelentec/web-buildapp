/**
* UserSkill.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var map = {
   user:{
     skill:{}
  } ,
  login:{
     skill:{}
  } ,
   profile:{
     skill:{}
  },
   myprofile:{
     skill:{}
  } 
};
module.exports = {
  tableName: 'UserSkill',
  attributes: { 
    
    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    owner: {
	  columnName: 'user_id',
      model: 'users'
    },
    skill: {type: 'integer'},
    getGrpFields: function(grp) {
        return map[grp];
    }
  }
}
