/**
* Mark.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


var map = {
    order: {
        id: {},
	name:{},
	text:{},
      city:{},
      skill:{},
      photos:{},
      owner:{},
      marked_by:{}
    },
      me: {
        id: {},
	name:{},
	text:{},
      city:{},
      skill:{},
      photos:{},
      owner:{},
      marked_by:{}
    },
      profile: {
        id: {},
	user_from_name:{},
	user_from_photo:{},
      mark:{},
      order_name:{},
      order_skill:{},
      createdAt:{},
      marked_by:{}
    },
         myprofile: {
        id: {},
	user_from_name:{},
	user_from_photo:{},
      mark:{},
      order_name:{},
      order_skill:{},
      createdAt:{},
      marked_by:{}
    }
};

module.exports = {

  tableName: 'Marks',
  attributes: {
  	id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    user_from_name: {type: 'string'},
     user_from_photo: {type: 'string'},
     mark: {type: 'integer'},
     order_id: {type: 'integer'},
     order_name: {type: 'string'},
    order_skill: {type: 'integer'},

       user: {
	  columnName: 'user_id',
      model: 'users'
    },  
      marked_by: {
	  columnName: 'marked_by',
      model: 'users'
    },  
  createdAt: {type: 'date'},

    getGrpFields: function(grp) {
        return map[grp];
    }
}
};
