/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var map = {
    login: {
        id: {},
        phone: {},
	  token: {},
	  name: {},
	 city: {},
	photo:{},
	role:{},
	 tariff_expire_time: {},
	 tariff: {},
		 userskills:{}
    },
      me: {
        id: {},
        phone: {},
	        name: {},
	        city: {},
		photo:{},
		 userskills:{}
    },
       profile: {
        id: {},
        phone: {},
	  name: {},
	 city: {},
	photo:{},
	 userskills:{},
	 marks:{}
    },
        myprofile: {
        id: {},
        phone: {},
	  name: {},
	 city: {},
	photo:{},
	 userskills:{},
	 marks:{},
	 orders:{},
	 tariff_expire_time: {},
	 tariff: {},
	 followed_orders:{}
    },
    mini_profile: {
        id: {},
        phone: {},
	  name: {},
	 city: {},
	photo:{},
      ban_expire:{}

    }
};

module.exports = {

  tableName: 'Users',
  attributes: {
  	id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    phone: {type: 'string'},
     code: {type: 'string'},
     token: {type: 'string'},
  os: {type: 'string'},
  push_token: {type: 'string'},
  tariff: {type: 'string'},
  countSms: {type: 'integer'},
lastSms: {type: 'datetime'},
tariff_expire_time: {type: 'datetime'},
    ban_expire:{type: 'datetime'},

     name: {type: 'string'},
       photo: {type: 'string'},
city: {type: 'integer'},
role: {type: 'integer'},
    userskills:  {
    	collection: 'userskill',
    	via: 'owner'
    },
     orders:  {
    	collection: 'orders',
    	via: 'owner'
    },
      followed_orders:  {
    	collection: 'followers',
    	via: 'user'
    },
      marks:  {
    	collection: 'mark',
    	via: 'user'
    },
      marks_to:  {
    	collection: 'mark',
    	via: 'marked_by'
    },
    getGrpFields: function(grp) {
        return map[grp];
    }
}
};
