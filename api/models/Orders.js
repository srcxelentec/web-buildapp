/**
* Orders.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


var map = {
    me: {
        id: {},
	name:{},
	text:{},
      city:{},
    //  skill:{},
      photos:{},owner:{},
      marked:{},
      createdAt:{},
      closed:{},
       skills: {},
      moderated:{},
      returned:{},
      return_text:{},

      followers_count:function() {
       return true;
    }
    },
      myprofile: {
        id: {}
    }
};

module.exports = {

  tableName: 'Orders',
  attributes: {
  	id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    name: {type: 'string'},
     text: {type: 'string'},
     skill: {type: 'integer'},
     city: {type: 'integer'},
          closed: {type: 'boolean'},
	    marked: {type: 'boolean'},
    moderated:{type: 'boolean'},
    returned:{type: 'boolean'},
    return_text:{type: 'string'},
    photos:  {
    	collection: 'orderphoto',
    	via: 'order'
    },
      skills:  {
    	collection: 'orderskill',
    	via: 'order'
    },
       owner: {
	  columnName: 'user_id',
      model: 'users'
    },
    followers:  {
    	collection: 'followers',
    	via: 'order'
    },
    createdAt: {type: 'date'},
    getGrpFields: function(grp) {
        return map[grp];
    }
}
};
